

let data = [
    {
        "id": 1,
        "post_id": 1,
        "user_id": 1,
        "comments": "Hello",
        "reply_id": null,
        "createdAt": "2019-09-04T08:40:34.000Z",
        "updatedAt": "2019-09-04T08:40:34.000Z"
    },
    {
        "id": 2,
        "post_id": 1,
        "user_id": 1,
        "comments": "Hii",
        "reply_id": 1,
        "createdAt": "2019-09-04T08:40:34.000Z",
        "updatedAt": "2019-09-04T08:40:34.000Z"
    },
    {
        "id": 3,
        "post_id": 1,
        "user_id": 1,
        "comments": "Hey",
        "reply_id": 1,
        "createdAt": "2019-09-04T08:40:34.000Z",
        "updatedAt": "2019-09-04T08:40:34.000Z"
    },
    {
        "id": 4,
        "post_id": 1,
        "user_id": 1,
        "comments": "Hey2",
        "reply_id": 3,
        "createdAt": "2019-09-04T08:40:34.000Z",
        "updatedAt": "2019-09-04T08:40:34.000Z"
    },
    {
        "id": 5,
        "post_id": 1,
        "user_id": 1,
        "comments": "OK",
        "reply_id": 2,
        "createdAt": "2019-09-04T08:40:34.000Z",
        "updatedAt": "2019-09-04T08:40:34.000Z"
    },
    {
        "id": 6,
        "post_id": 1,
        "user_id": 1,
        "comments": "Hh",
        "reply_id": null,
        "createdAt": "2019-09-04T08:40:34.000Z",
        "updatedAt": "2019-09-04T08:40:34.000Z"
    },
    {
        "id": 7,
        "post_id": 1,
        "user_id": 1,
        "comments": "Hee",
        "reply_id": 6,
        "createdAt": "2019-09-04T08:40:34.000Z",
        "updatedAt": "2019-09-04T08:40:34.000Z"
    },
    {
        "id": 8,
        "post_id": 1,
        "user_id": 1,
        "comments": "Hii",
        "reply_id": 4,
        "createdAt": "2019-09-04T08:40:34.000Z",
        "updatedAt": "2019-09-04T08:40:34.000Z"
    },
]

let data2 = data.filter(({ reply_id }) => reply_id === null);


for (let i = 0; i < data.length; i++) {
    if (data[i].reply_id != null) {
        for (let j = 0; j < data2.length; j++) {
            if (data[i].reply_id === data2[j].id) {
                data2[j].replies = data[i];
            }
        }
    }
}

console.log(data2);