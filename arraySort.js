function bubbleSort(array) {
    let done = false;
    while (!done) {
        done = true;
        for (let i = 1; i < array.length; i++) {
            if (array[i - 1] > array[i]) {
                done = false;
                let tmp = array[i - 1];
                array[i - 1] = array[i];
                array[i] = tmp;
            }
        }
    }

    return array;
}

let numbers = [4, 2, 4, 3, 1, 5];
bubbleSort(numbers);
console.log(numbers);

function bubbleSortTwo(arrays) {
    let done = false;
    while (!done) {
        done = true;
        for (let i = 1; i < arrays.length; i++) {
            if (arrays[i - 1] > arrays[i]) {
                done = false;
                let tmp = arrays[i - 1];
                arrays[i - 1] = arrays[i];
                arrays[i] = tmp;
            }
        }
    }

    return arrays;
}

let number = [0, 1, 2, 4, 8, 5, 8, 4, 2, 1, 0];
bubbleSortTwo(number);
console.log(number);